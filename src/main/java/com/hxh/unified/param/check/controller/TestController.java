package com.hxh.unified.param.check.controller;

import com.hxh.unified.param.check.form.ExampleForm;
import com.hxh.unified.param.check.form.RequestForm;
import com.hxh.unified.param.check.utils.ResultVoUtil;
import com.hxh.unified.param.check.vo.ResultVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author huangxunhui
 * Date: Created in 2020/3/4 6:18 下午
 * Utils: Intellij Idea
 * Description:
 */
@RestController
public class TestController {

    /**
     * 表单请求
     * @param form 请求参数
     * @return 响应数据
     */
    @GetMapping("/formRequest")
    public ResultVo formRequest(@Validated RequestForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * JSON请求
     * @param form 请求参数
     * @return 响应数据
     */
    @PostMapping("/jsonRequest")
    public ResultVo jsonRequest(@RequestBody @Validated RequestForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * 案例测试
     * @param form 请求参数
     * @return 响应数据
     */
    @PostMapping("/exampleTest")
    public ResultVo jsonRequest(@RequestBody @Validated ExampleForm form){
        return ResultVoUtil.success(form);
    }

}
