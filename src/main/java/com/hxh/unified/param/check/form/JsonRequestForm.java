package com.hxh.unified.param.check.form;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 2020/3/4 9:20 下午
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class JsonRequestForm {

    @Valid
    @Size(min = 1 ,max =  10 , message = "列表中的元素数量为1~10")
    private List<RequestForm> requestFormList;

}
